package com.forever.jfinal;

import com.jfinal.plugin.activerecord.Model;

import java.sql.Timestamp;

/**
 * Created by Xuemeng Wang on 15-7-3.
 */
public class User extends Model<User> {
    public static final User dao = new User();

    private String guid;
    private Timestamp create_time;
    private int archived;
    private int version;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Timestamp last_login_time;
}
