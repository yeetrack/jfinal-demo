package com.forever.jfinal.controller;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.log.Logger;

public class HelloController extends Controller {
    private static final Logger logger = Logger.getLogger(HelloController.class);

	public void index() {
        logger.debug(getPara("name"));
		renderText("Hello JFinal World.");
	}

	@ActionKey("/checkpreload")
	public void hello() {
		renderText("success");
	}
}