package com.forever.jfinal.controller;

import com.forever.jfinal.User;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Xuemeng Wang on 15-7-3.
 */
public class UserController extends Controller {

    public void save() {
        User user = getModel(User.class);

        boolean result = user.save();
        renderText("" + result);
    }

    public void delete() {
        String id = getPara("id");
        boolean result = User.dao.deleteById(id);
        renderText(""+result);
    }

    public void find() {
        List<User> users = User.dao.find("select * from user_");
        renderJson(users);
    }

    public void findByPage() {
        Page<User> userPage = User.dao.paginate(2, 4, "select *", "from user_");
        renderJson(userPage.getList());
    }

    public void testDB() {
        int id = getParaToInt("id");
        Record record = Db.findFirst("select * from user_ where id = ?", id);

        List<User> users = User.dao.find("select * from user_");

        setAttr("user", "Nini");
        setAttr("users", users);
        renderFreeMarker("/index.ftl");
    }

}
