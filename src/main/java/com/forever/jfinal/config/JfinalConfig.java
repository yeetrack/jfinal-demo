package com.forever.jfinal.config;

import com.forever.jfinal.controller.HelloController;
import com.forever.jfinal.controller.UserController;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.UrlSkipHandler;

public class JfinalConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/hello", HelloController.class);
        me.add("/user", UserController.class);

	}

	@Override
	public void configPlugin(Plugins me) {
//        loadPropertyFile("jdbc.properties");
 //      C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("urlMaster"), getProperty("username"), getProperty("password"));
   //     me.add(c3p0Plugin);

     //   ActiveRecordPlugin activeRecordPlugin = new ActiveRecordPlugin(c3p0Plugin);
      //  me.add(activeRecordPlugin);
       // activeRecordPlugin.addMapping("user_", User.class);
	}

	@Override
	public void configInterceptor(Interceptors me) {

	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new UrlSkipHandler("/checkpreload.htm", false));
	}

}
